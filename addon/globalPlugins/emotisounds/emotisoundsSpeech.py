# -*- coding: UTF-8 -*-
#emotisoundsSpeech.py
#A part of Emoti Sounds addon for NVDA
#This file is covered by the GNU General Public License.
#See the file COPYING for more details.
#Copyright (C) 2015-2016 Hadrons Tecnologia LTDA <contact@hadrons.io>
#This file uses part of the NVDA speech module, specificaly the speak method (Copyright (C) 2006-2014 NV Access Limited, Peter Vágner, Aleksey Sadovoy)

"""Main NVDA related functions to play audio at the right moment 
"""

from threading import Timer
import config
import controlTypes
from logHandler import log
import speech
import synthDriverHandler
import synthDrivers
import ui
import audioFiles
import audioPlayer
import emotisoundsInterceptor
import textHandler

preparedSpeechSequence = []
lastSpeechId = 0

def isESpeakActive():
	currentDriver = synthDriverHandler.getSynth()
	return currentDriver.name == 'espeak'

def speakAudio(speechId):
	global preparedSpeechSequence, lastSpeechId
	if not isESpeakActive():
		emotisoundsInterceptor.originalSpeakReference(preparedSpeechSequence)
		return
	if lastSpeechId == speechId:
		if synthDrivers._espeak.isSpeaking:
			Timer(0.1, speakAudio, [speechId]).start()
		elif len(preparedSpeechSequence) > 0:
			currentSpeechItem = preparedSpeechSequence[0]
			if isinstance(currentSpeechItem, basestring) and currentSpeechItem in audioFiles.audioIndexReferente:
				currentSpeechItemIndex = audioFiles.audioIndexReferente.index(currentSpeechItem)
				audioPlayer.play(audioFiles.audioReferenceList[currentSpeechItemIndex])
				preparedSpeechSequence.pop(0)
				Timer(0.1, speakAudio, [speechId]).start()
			else:
				emotisoundsInterceptor.originalSpeakReference([preparedSpeechSequence[0]])
				preparedSpeechSequence.pop(0)
				Timer(0.1, speakAudio, [speechId]).start()

def speak(speechSequence,symbolLevel=None):
	"""Method to check if there is any emoji in the text that should be read by the synthesizer
	@param speechSequence: the sequence of text and L{SpeechCommand} objects to speak
	@param symbolLevel: The symbol verbosity level; C{None} (default) to use the user's configuration.
	"""
	global preparedSpeechSequence, lastSpeechId
	lastSpeechId += 1
	preparedSpeechSequence = textHandler.getPreparedSpeechSequence(speechSequence)
	Timer(0.1, speakAudio, [lastSpeechId]).start()
