# -*- coding: UTF-8 -*-
#__init__.py
#A part of Emoti Sounds addon for NVDA
#This file is covered by the GNU General Public License.
#See the file COPYING for more details.
#Copyright (C) 2015-2016 Hadrons Tecnologia LTDA <contact@hadrons.io>

"""Handles the addon activation
"""

import addonHandler
import globalPluginHandler
import ui
import emotisoundsInterceptor
import emotisoundsSpeech

addonHandler.initTranslation()

PLUGIN_STATE_ON = 0
PLUGIN_STATE_OFF = 1

class GlobalPlugin(globalPluginHandler.GlobalPlugin):
	__gestures = {
		"kb:NVDA+shift+h": "togglePluginState",
	}
	__pluginState = PLUGIN_STATE_OFF

	def script_togglePluginState(self, gesture):
		if self.__pluginState == PLUGIN_STATE_ON:
			self.disableEmotisounds()
		else:
			self.enableEmotisounds()

	def enableEmotisounds(self):
		if not emotisoundsSpeech.isESpeakActive():
			# Translators: message presented when the eSpeak driver is not selected
			ui.message(_("Please, use the eSpeak driver"))
		else:
			self.__pluginState = PLUGIN_STATE_ON
			emotisoundsInterceptor.enable()
			# Translators: message presented when the emotisounds addon is loaded.
			ui.message(_("Emoti Sounds On."))

	def disableEmotisounds(self):
		self.__pluginState = PLUGIN_STATE_OFF
		emotisoundsInterceptor.disable()
		# Translators: message presented when the emotisounds addon is unloaded.
		ui.message(_("Emoti Sounds Off."))
