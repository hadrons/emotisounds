# -*- coding: UTF-8 -*-
#textHandler.py
#A part of Emoti Sounds addon for NVDA
#This file is covered by the GNU General Public License.
#See the file COPYING for more details.
#Copyright (C) 2015-2016 Hadrons Tecnologia LTDA <contact@hadrons.io>

"""Handles text operations for the speechSequence
"""

import hashlib
import random
import re
import audioFiles

SEPARATOR = hashlib.sha224(str(random.random())).hexdigest()

def getCharSearch(audioReferenceList):    
	charsList = []
	for audioReference in audioReferenceList:
		charsList.append(audioReference.char)
	groupedChars = "|".join(charsList)
	return re.compile("(?P<chars>%s)" % groupedChars)

def getPreparedText(text):
	return charSearch.sub("%s\\g<chars>%s" % (SEPARATOR, SEPARATOR), text)

def getTextList(text):
	return getPreparedText(text).split(SEPARATOR)

def getPreparedSpeechSequence(speechSequence):
	preparedSpeechSequence = []
	for item in speechSequence:
		if isinstance(item, basestring):
			preparedSpeechSequence += getTextList(item)
		else:
			preparedSpeechSequence.append(item)
	return preparedSpeechSequence

charSearch = getCharSearch(audioFiles.audioReferenceList)
