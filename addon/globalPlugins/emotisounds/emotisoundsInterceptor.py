# -*- coding: UTF-8 -*-
#emotisoundsSpeech.py
#A part of Emoti Sounds addon for NVDA
#This file is covered by the GNU General Public License.
#See the file COPYING for more details.
#Copyright (C) 2015-2016 Hadrons Tecnologia LTDA <contact@hadrons.io>

"""Monkey patching speech.speak method
"""

import speech
import emotisoundsSpeech

originalSpeakReference = speech.speak

def enable():
    originalSpeakReference = speech.speak
    speech.speak = emotisoundsSpeech.speak

def disable():
    speech.speak = originalSpeakReference
