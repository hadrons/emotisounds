# -*- coding: UTF-8 -*-
#audioPlayer.py
#A part of Emoti Sounds addon for NVDA
#This file is covered by the GNU General Public License.
#See the file COPYING for more details.
#Copyright (C) 2015-2016 Hadrons Tecnologia LTDA <contact@hadrons.io>

"""Handles the waves playback
"""

import os
import time
import globalVars
import nvwave

AUDIO_INTERVAL = 1

path = os.path.join(globalVars.appArgs.configPath, "addons", "emotisounds", "globalPlugins", "emotisounds", "waves")

def play(audioReference):
	nvwave.playWaveFile(os.path.join(path, audioReference.audio))
	time.sleep(audioReference.length + AUDIO_INTERVAL)
