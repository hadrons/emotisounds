# Emoti Sounds

Meet the Emoti Sounds. The plugin that translates the name of an emoticon to an audio that corresponds to the real emotion it represents.

All audio files in this work are licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

## Update History
- 2.4
    - Bug fixes
    - Added sounds for Facebook emoticons:
        - French (Canada and France)
        - Spanish (Spain and Spanish - General)
        - Italian
        - English (US and UK)
- 2.3
    - Added eSpeak driver verification
- 2.2
    - Fixed bug that made the addon repeat some texts
    - Code standardization
- 2.1
    - Updating audio files
- 2.0
    - Addon update to meet NVDA addonTemplate
- v1.1.0
    - New audios!
- v1.0.3
    - Bug fixes
- v1.0.2
    - Audio update
- v1.0.1
    - Facebook integration for English and Brazilian Portuguese
- v1.0.0
    - First release